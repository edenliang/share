/**
 * Created by EdenLiang on 2016/8/10.
 */
'use strict';
var fs = require('fs');
var path = require('path');
var webpack = require('webpack');

var config = {
    devtool: 'evel',
    //入口文件
    entry: {
        app: ['webpack-dev-server/client?http://localhost:3008', 'webpack/hot/dev-server', './src/index']
    },
    //输出文件
    output: {
        path: path.join(__dirname, 'public'),
        publicPath: '/public/',
        chunkFilename: '[id].chunk.js',
        filename: 'bundle.js'
    },
    //加载器
    module: {
        loaders: [
            {test: /\.css$/, loader: 'style!css'},
            //图片文件使用 url-loader 来处理，小于8kb的直接转为base64
            {test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192'},
            //eslint验证
            {test: /\.js$/, loader: 'eslint', exclude: /node_modules/},
            {test: /\.js$/, loaders: ['react-hot', 'babel'], include: [path.join(__dirname, 'src')]}
        ]
    },
    //插件
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        //new webpack.ProvidePlugin({
        //    "$": __dirname + "/src/static/lib/jquery-3.1.js"
        //}),
        new webpack.optimize.CommonsChunkPlugin('shared.js'),
        new webpack.DefinePlugin({
            'process.env': {
                'DEBUG': true
            }
        })
    ]
};
module.exports = config;