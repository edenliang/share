/**
 * Created by Eden on 2016/8/15.
 */
'use strict';
import React from 'react';
import { createDevTools } from 'redux-devtools';
import DockMonitor from 'redux-devtools-dock-monitor';
import LogMonitor from 'redux-devtools-log-monitor';
import SliderMonitor from 'redux-slider-monitor';

const DevTools = createDevTools(
    <DockMonitor toggleVisibilityKey='ctrl-h'
                 changePositionKey='ctrl-q'
                 defaultPosition='right'>
        <LogMonitor theme='tomorrow'/>
    </DockMonitor>
);
//选一个吧
//                 defaultSize={0.15}
//theme: tomorrow,solarized
//<LogMonitor theme='solarized'/>
//<SliderMonitor keyboardEnabled/>

export default DevTools;