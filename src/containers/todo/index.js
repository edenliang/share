/**
 * Created by Eden on 2016/8/15.
 */
'use strict';

import React,{Component} from 'react';
import { connect } from 'react-redux'
import { TodoAction } from '../../action';
import AddTodo from '../../components/todo/add';
import TodoList from '../../components/todo/list';
import _ from 'lodash';

class Todo extends Component {
    constructor(props) {
        super(props);
    }

    confirmTodo(item) {
        item.id = _.uniqueId();
        item.rate = 0;
        const { dispatch } = this.props;
        dispatch(TodoAction.AddTodo(item));
    }

    changeRate(item){
        const { dispatch } = this.props;
        dispatch(TodoAction.CHANGERATE(item));
    }

    render() {
        const { list } = this.props;
        return (<div>
            <AddTodo confirm={(item)=>this.confirmTodo(item)}  />
            <TodoList list={list} changeRate={(item)=>this.changeRate(item)} />
        </div>)
    }

}


export default connect((state)=> {
    return {list: state.Todo.list};
})(Todo);