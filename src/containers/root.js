/**
 * Created by Eden on 2016/8/15.
 */
'use strict';
import React from 'react';
import { browserHistory, Router,Route } from 'react-router'
import { Provider } from 'react-redux';
import ConfigureStore from '../store';
import DevTools from './devTools';
//import Container from './';
//import Todo from './todo';
const store = ConfigureStore();


const routes = {
    path: '/',
    component: require('./').default,
    childRoutes: [
        {
            path: 'todo',
            getComponent: (nextState, cb) => {
                require.ensure([], (require) => {
                    cb(null, require('./todo').default)
                })
            }
        }
    ]
};


const Root = () => {
    return (<span>
        <Provider store={store}>
            <Router routes={routes} history={browserHistory}/>
        </Provider>
        <DevTools store={store}/>
    </span>)
};

export default Root;
