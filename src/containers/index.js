/**
 * Created by Eden on 2016/8/15.
 */
'use strict';
import React,{ Component } from 'react';
import { Link } from 'react-router';
import Left from '../components/left';
import '../static/style/index.css';

class Container extends Component {
    render() {
        const { children } = this.props;
        return (<div>
            <div className="top">
                <span style={{fontSize:'20px'}}>技术分享</span>
            </div>
            <div className="left">
                <Left />
            </div>
            <div className="right">{children}</div>
        </div>)
    }
}


export default Container;