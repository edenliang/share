/**
 * Created by Eden on 2016/8/15.
 */
'use strict';
import React from 'react';
import ReactDOM from 'react-dom';
import 'babel-polyfill';
import Root from './containers/root';


ReactDOM.render(<Root />, document.getElementById('wrapper'));
