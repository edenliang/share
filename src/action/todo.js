/**
 * Created by Eden on 2016/8/15.
 */
'use strict';
import { TODOS } from '../constants';

export function AddTodo(item) {
    return {type: TODOS.ADD, item};
}

export function CHANGERATE(item) {
    return {type: TODOS.CHANGERATE, item};
}