/**
 * Created by Eden on 2016/8/15.
 */
'use strict';

import { TODOS } from '../constants';

const initState = {
    list: []
};


const Todos = (state = initState, action) => {
    switch (action.type) {
        case TODOS.ADD:
            return Object.assign({}, state, {list: [...state.list, action.item]});
        case TODOS.CHANGERATE:
            return Object.assign({}, state, {list: state.list.map(item=>item.id == action.item.id ? Object.assign({}, item, {rate: action.item.rate}) : item)});
        default:
            return state;
    }
};

export default Todos;

// return Object.assign({}, state, {items: state.items.map(item=>item.id == action.item.id ? Object.assign({}, item, action.item) : item)});