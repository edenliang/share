/**
 * Created by Eden on 2016/8/15.
 */
'use strict';
import { combineReducers } from 'redux';
import Todo from './todos';

const Reducer = combineReducers({
    Todo
});

export default Reducer;
