/**
 * Created by Eden on 2016/8/16.
 */
'use strict';
import React from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router';

const Left = () => {
    return (
        <Menu style={{width:230}} defaultOpenKeys={['sub1','sub2']} mode="inline">
            <Menu.SubMenu key="sub1" title={<span>分享demo</span>}>
                <Menu.Item><Link to="/todo">todo</Link></Menu.Item>
            </Menu.SubMenu>
        </Menu>
    );
};
export default Left;
