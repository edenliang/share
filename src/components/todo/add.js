/**
 * Created by Eden on 2016/8/15.
 */
'use strict';
import React,{ Component } from 'react';
import { Form,Input,Button } from 'antd';
const createForm = Form.create;
const FormItem = Form.Item;

class AddTodo extends Component {
    render() {
        const { confirm } = this.props;
        const { getFieldProps } = this.props.form;
        return (<Form inline>
            <FormItem label="任务">
                <Input placeholder="请输入任务"
                    {...getFieldProps('text')}
                />
            </FormItem>
            <Button type="primary" onClick={()=>confirm(this.props.form.getFieldsValue())}>确定</Button>
        </Form>)
    }
}


AddTodo = createForm()(AddTodo);
export default AddTodo;