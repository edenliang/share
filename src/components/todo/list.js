/**
 * Created by Eden on 2016/8/15.
 */
'use strict';
import React,{ Component } from 'react';
import _ from 'lodash';
import { Table,Rate } from 'antd';


const TodoList = ({list,changeRate}) => {
    const columns = [{
        title: '事件',
        dataIndex: 'text',
        key: 'text'
    }, {
        title: '打分',
        key: 'rate',
        render: (text, record) =><Rate allowHalf onChange={(value)=>{record.rate=value; changeRate(record)}} value={record.rate}/>
    }];

    return (<Table columns={columns} dataSource={list} pagination={false}/>)
};


export default TodoList;