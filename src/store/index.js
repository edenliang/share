/**
 * Created by Eden on 2016/8/15.


 */
'use strict';
import { createStore,applyMiddleware,compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import Reducer from '../reducers';
import { persistState } from 'redux-devtools';
import DevTools from '../containers/devTools';

const createStoreWithMiddleware = process.env.NODE_ENV == 'production1' ? applyMiddleware(thunkMiddleware)(createStore) : compose(
    applyMiddleware(thunkMiddleware),
    DevTools.instrument(),
    persistState(
        window.location.href.match(
            /[?&]debug_session=([^&#]+)\b/
        )
    )
)(createStore);

export default function configureStore(state) {
    return createStoreWithMiddleware(Reducer, state);
}

